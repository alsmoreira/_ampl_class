# An automobile manufactorer produces several kinds of cars;
# Each cars requires a certain amount of factory time (time_rate) per car to produce
# and yelds a certain profit (profit_car) per car.
# A certain amount of factory time has been scheduled for the next week (avail).

set KIND;						# Kinds of cars

param time_car {KIND} >= 0;
param profit_car {KIND} >= 0;
param avail >= 0;

param orders {KIND} >= 0;		# upper limit on tons sold in week

var Make {k in KIND} >= 0, <= orders[k];	#tons produced

maximize Total_Profit: sum {k in KIND} profit_car[k] * Make[k];
		# Objective : total profits for all products

subject to Time: sum {k in KIND} time_car[k] * Make[k] <= avail;
		# Constraint: total of hours used by all
		# products may not exceed hours available