# P = a set of products
# a_j = tons per hour of product j, for each j belongs P
# b = hour available at the mill
# c_j = profit per ton of product j, for each j belongs P
# u_j = maximum tons of product j, for each j belongs P

# Variables
# X_j = tons of product j to be made, for each j belongs P
# Maximize 
# sum_{j belongs P} c_j * X_j
# Subject to:
# sum_{j belongs P} (1/a_j) * X_j <= b
# 0 <= X_j <= u_j, for each j belongs P

set P;

param a {j in P};
param b;
param c {j in P};
param u {j in P};

var X {j in P};

maximize Total_Profit: sum {j in P} c[j] * X[j];

subject to Time: sum {j in P} (1/a[j]) * X[j] <= b;
subject to Limit {j in P}: 0 <= X[j] <= u[j]; 