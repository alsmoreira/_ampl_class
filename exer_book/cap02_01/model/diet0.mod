# Autor: Anderson Moreira
# Considerar o problema de escolher alimentos preparados para
# atender certas necessidades nutricionais.
# Suponha que alguns tipos de alimentos est�o 
# dispon�veis com os seguintes pre�os por pacote:
#
# TO-DO - Pesquisar valores reais em Supermercados
#
# BEEF	carne	R$	3.19
# CHK	frango		2.59
# FISH	peixe		2.29
# HAM	presunto 	2.89
# MCH	massa		1.89
# MTL	carne moida	1.99
# SPG	molho		1.99
# TUR	peito peru	2.49
#
# Esses jantares forne�em as seguintes percentagens, por pacote, 
# de requisitos m�nimos di�rios de vitaminas A, C, B1 and B2:

# 		A	C	B1	B2
# BEEF	60%	20%	10%	15%
# CHK 	8 	0 	20 	20
# FISH 	8 	10 	15 	10
# HAM 	40 	40 	35 	10
# MCH 	15 	35 	15 	15
# MTL 	70 	30 	15 	15
# SPG 	25 	50 	25 	15
# TUR 	60 	20 	15 	10

# O problema � encontrar a combina��o mais barata de alimentos
# para suprir os requisitos de uma semana - que �, no m�nimo, 700% (chute) da necessidade
# di�ria de cada nutriente.

var Xbeef >= 0; 
var Xchk >= 0; 
var Xfish >= 0;
var Xham >= 0; 
var Xmch >= 0; 
var Xmtl >= 0;
var Xspg >= 0; 
var Xtur >= 0;

minimize cost:
3.19*Xbeef + 2.59*Xchk + 2.29*Xfish + 2.89*Xham + 1.89*Xmch + 1.99*Xmtl + 1.99*Xspg + 2.49*Xtur;

subject to A:
60*Xbeef + 8*Xchk + 8*Xfish + 40*Xham + 15*Xmch + 70*Xmtl + 25*Xspg + 60*Xtur = 700;

subject to C:
20*Xbeef + 0*Xchk + 10*Xfish + 40*Xham + 35*Xmch + 30*Xmtl + 50*Xspg + 20*Xtur = 700;

subject to B1:
10*Xbeef + 20*Xchk + 15*Xfish + 35*Xham + 15*Xmch + 15*Xmtl + 25*Xspg + 15*Xtur = 700;

subject to B2:
15*Xbeef + 20*Xchk + 10*Xfish + 10*Xham + 15*Xmch + 15*Xmtl + 15*Xspg + 10*Xtur = 700;