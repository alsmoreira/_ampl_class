# max_budget = $ 1000000
# 1 min TV = $ 20000 - impact 1.8 M customers
# 1 page Magazine = $ 10000 - impact 1 M customers
# min_time_TV >= 10 min
# min_page_mg >= 0 pages
# objective: maximize audience
# X_tv = number time for audience on TV
# X_mg = number pages for audience on magazine
# audience = (1.8) * TV + (1) * Magazine
# constraints:
# budget -> (0.02 * TV) + (0.01 * Magazine) <= 1

# b)
# 3 person/week = 1 page magazine
# 1 person/week = 1 minute TV

# c)
# Radio 250000 people/minute - cost $ 2000 / minute - 1 person/week

# d)
# Min 2 magazines pages and 120 minutes of radio

var X_tv >= 10;
var X_mg >= 2;
var X_rd >= 0, <= 120;

maximize audience: (1.8 * X_tv) + (1 * X_mg) + (0.25 * X_rd);

subject to budget: (0.02 * X_tv) + (0.01 * X_mg) + (0.002 * X_rd) <= 1;
subject to create: (1 * X_tv) + (3 * X_mg) + (1 * X_rd) <= 100;