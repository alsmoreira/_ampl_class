#MINOS is a solver for linear & nonlinear problems in continuous variables;
#CPLEX and FortMP are for linear & convex quadratic problems in continuous
#and/or integer variables.  So your choice must depend on the kind of problem
#that you're trying to solve.

set PROD;						# products
set STAGE;						# stages of production

param rate {PROD, STAGE} > 0;			# tons produced per hour
param avail {STAGE} >= 0;				# hours available in week
param profit {PROD};			# profit per ton

param market {PROD} >= 0;		# upper limit on tons sold in week
param commit {PROD} >= 0;		# lower limit on tons sold in week

var Make {p in PROD} >= commit[p], <= market[p];	#tons produced

maximize Total_Profit: sum {p in PROD} profit[p] * Make[p];
		# Objective : total profits for all products

s.t. Time {s in STAGE}: sum {p in PROD} (1/rate[p,s]) * Make[p] <= avail[s];
		# Constraint: total of hours used by all
		# products may not exceed hours available