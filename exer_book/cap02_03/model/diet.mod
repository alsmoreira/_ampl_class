# Conjunto de nutrientes
set NUTR;
# Conjunto de alimentos
set FOOD;

# Custo por alimento tem de ser maior que zero, pois se for igual � de gra�a!
param cost {FOOD} > 0;

# Limites minimos e maximos de 
param f_min {FOOD} >= 0;
param f_max {f in FOOD} >= f_min[f];

param n_min {NUTR} >= 0;
param n_max {n in NUTR} >= n_min[n];

param amt {NUTR,FOOD} >= 0;

var Buy {f in FOOD} integer >= f_min[f], <= f_max[f];

minimize Total_Cost: sum {f in FOOD} cost[f] * Buy[f];

subject to Diet {n in NUTR}:
	n_min[n] <= sum {f in FOOD} amt[n,f] * Buy[f] <= n_max[n];