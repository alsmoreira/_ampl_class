set STORES;
set PORTS;

param availability { STORES } >= 0;
param demand { PORTS } >= 0;
param distance { STORES, PORTS } >= 0;
param costkm >= 0;

var x { STORES, PORTS } >= 0;
var y { STORES, PORTS } >= 0, integer;

minimize cost:
	sum {i in STORES, j in PORTS} costkm * distance[i,j] * y[i,j];

subject to avail {i in STORES}:
	sum {j in PORTS} x[i,j] <= availability[i];

subject to request {j in PORTS}:
	sum {i in STORES} x[i,j] >= demand[j];

subject to lorrycap {i in STORES, j in PORTS}:
	2*y[i,j] >= x[i,j];