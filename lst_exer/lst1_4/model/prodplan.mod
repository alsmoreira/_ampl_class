set PROD;

param Months;

set MONTHS := 1..Months;
set MONTHS0 := MONTHS union {0};

param days {MONTHS} >= 0;
param demand {PROD, MONTHS} >= 0;
param price {PROD} >= 0;
param cost {PROD} >= 0;
param quota {PROD} >= 0;
param activation {PROD} >= 0;
param batch {PROD} >= 0;
param storage {PROD} >= 0;
param capacity >= 0;

var x {PROD, MONTHS} >= 0;
var w {PROD, MONTHS} >= 0;
var z {PROD, MONTHS0} >= 0;
var bol {PROD, MONTHS} >= 0, binary, integer;

maximize revenue:
	sum {i in PROD} (price[i] * sum {j in MONTHS} w[i,j] - cost[i] * sum {j in MONTHS} x[i,j] - storage[i] * sum {j in MONTHS} z[i,j] - activation[i] * sum {j in MONTHS} bol[i,j]);

subject to requirement {i in PROD, j in MONTHS}:
	w[i,j] <= demand[i,j];

subject to production {j in MONTHS}:
	sum {i in PROD} (x[i,j] / quota[i]) <= days[j];

subject to bilance {i in PROD, j in MONTHS}:
	z[i,j-1] + x[i,j] = z[i,j] + w[i,j];

subject to capacitymag {j in MONTHS}:
	sum {i in PROD} z[i,j] <= capacity;

subject to active {i in PROD, j in MONTHS}:
	x[i,j] <= days[j]*quota[i]*bol[i,j];

subject to minbatch {i in PROD, j in MONTHS}:
	x[i,j] >= batch[i]*bol[i,j];