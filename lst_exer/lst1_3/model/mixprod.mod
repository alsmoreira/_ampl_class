set PROD;

param days >= 0;
param demand {PROD} >= 0;
param price {PROD} >= 0;
param cost {PROD} >= 0;
param quota {PROD} >= 0;
param act_cost {PROD} >= 0; # custo de ativa��o
param min_batch {PROD} >= 0; # lotes m�nimos

param lower {PROD} >= 0;
param upper {PROD} >= 0;

var q_prod {i in PROD} >= lower[i] integer, <= upper[i] integer; # quantidade de produtos
var bol {PROD} >= 0, binary; # ativa��o de uma linha de produ��o - 1 = ativada, 0 = desativada

maximize profit: 
	sum {i in PROD}((price[i] - cost[i]) * q_prod[i] - act_cost[i] * bol[i]);

subject to requirement {i in PROD}:
	q_prod[i] <= demand[i];

subject to production:
	sum {i in PROD} (q_prod[i] / quota[i]) <= days;

subject to activation {i in PROD}:
	q_prod[i] <= days * quota[i] * bol[i];

subject to batches {i in PROD}:
	q_prod[i] >= min_batch[i] * bol[i];