param n > 0;
param m > 0;

set N := 1..n;
set M := 1..m;

param lambda{N} >= 0;
param mu := sum{i in N} lambda[i];
param L >= 0;

var x{N,M} binary;
var t{N} >= 0;
var y{M} binary;
var T >= 0;

minimize minmaxobj: T;

subject to minmax {j in M} : T >= t[j];

subject to carlinedef {j in M} :
	t[j] = sum{i in N} lambda[i] * x[i,j];

subject to assignment {i in N} : sum{j in M} x[i,j] = 1;

## second question
#subject to disjunction {j in M} : t[j] <= L + mu * (1 - y[j]);
#subject to onelineonly : sum{j in M} y[j] = 1;