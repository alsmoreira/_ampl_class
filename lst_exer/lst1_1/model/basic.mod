# 1 - Quest�o
# Observe o seguinte exemplo de um problema de programa��o linear:
# minimize z= -x1 - 2x2
# subject to 	-2x1 + x2 <= 2,
#				-x1 + x2 <= 3,
#				x1 <= 3
#				x1, x2 >= 0

var x1 >= 0;
var x2 >= 0;

minimize Z: -x1 -2*x2;
	
subject to R1: -2*x1 +x2 <= 2;

s.t. R2: -x1 + x2 <= 3;
	
s.t. R3: x1 <= 3; 