# 2 - Quest�o
# Uma Empresa de metais fabrica quatro diferentes ligas de quatro metais b�sicos. 
# O objetivo � determinar a melhor mistura de produtos para maximizar a receita bruta sem
# ultrapassa os limites de produto ofertado. Os requisitos s�o dados na tabela abaixo. Formular
# um modelo de otimiza��o linear para o problema.

set alloy;
set metal;

param limit {metal};
param price {alloy};
param proportion_1 {i in metal};
param proportion_2 {i in metal};
param proportion_3 {i in metal};
param proportion_4 {i in metal};

param lower {alloy} >= 0;
param upper {alloy} >= 0;

var amount {i in alloy} >= lower[i], <= upper[i];

maximize optimalMix:
	sum {i in alloy} price[i]*amount[i];
	
s.t. metal_supply_1:
	sum {i in alloy}(proportion_1[i]*amount[i]) <= 5;

s.t. metal_supply_2:
	sum {i in alloy}(proportion_2[i]*amount[i]) <= 5;

s.t. metal_supply_3:
	sum {i in alloy}(proportion_3[i]*amount[i]) <= 1;
	
s.t. metal_supply_4:
	sum {i in alloy}(proportion_4[i]*amount[i]) <= 2;