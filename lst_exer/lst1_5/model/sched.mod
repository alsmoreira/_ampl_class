param p > 0, integer;
param c > 0, integer;

set P := 1..p;
set C := 1..c;

param b{P} >= 0;
param s{C} >= 0;
param Wmax default sum{i in P} b[i] / (min{k in C} s[k]);

var x{P} >= 0; # starting time of task
var y{P} >= 0; # CPU of task
var z{P,C} binary; # assignment task/CPU
var sigma{P,P} binary; # is task i scheduled before task j?
var epsilon{P,P} binary; # is task i scheduled on a lower CPU than task j?
var L{P} >= 0; # length of task
var W >= 0; # makespan

minimize makespan: W;

subject to times{i in P} : W >= x[i] + L[i];

subject to lengths{i in P} : L[i] = sum{k in C} (b[i]/s[k]) * z[i,k];

subject to assignment{i in P} : sum{k in C} z[i,k] = 1;

subject to cpudef{i in P} : y[i] = sum{k in C} k * z[i,k];

subject to hnonoverlapping{i in P, j in P : i != j} :
	x[j] - x[i] >= L[i] - (1-sigma[i,j]) * Wmax;

subject to vnonoverlapping{i in P, j in P : i != j} :
	y[j] - y[i] >= 1 - (1-epsilon[i,j]) * card(P);

subject to atleastone{i in P, j in P : i != j} :
	sigma[i,j] + sigma[j,i] + epsilon[i,j] + epsilon[j,i] >= 1;
	
subject to hatmostone{i in P, j in P : i != j} :
	sigma[i,j] + sigma[j,i] <= 1;

subject to vatmostone{i in P, j in P : i != j} :
	epsilon[i,j] + epsilon[j,i] <= 1;